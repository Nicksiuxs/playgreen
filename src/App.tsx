import { Routes, Route } from 'react-router-dom';
import PrivateRoutes from './components/PrivateRoutes/PrivateRoutes';
import UserRoutes from './routes/UserRoutes';
import Login from './pages/Login/Login';
import Singup from './pages/Signup/Singup';
import ResetPassword from './pages/ResetPassword/ResetPassword';

function App() {
	return (
		<Routes>
			<Route path="login" element={<Login />} />
			<Route path="signup" element={<Singup />} />
			<Route path="reset-password" element={<ResetPassword />} />
			<Route
				path="/*"
				element={
					<PrivateRoutes>
						<UserRoutes />
					</PrivateRoutes>
				}
			/>
		</Routes>
	);
}

export default App;
