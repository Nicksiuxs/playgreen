import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  *,
  *::before,
  *::after {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body{
    max-height: 100vh !important;
    max-width: 100vw !important;
  }
`;

export default GlobalStyle;
