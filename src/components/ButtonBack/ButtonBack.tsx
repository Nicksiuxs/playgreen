import React from 'react';
import { Button } from './ButtonBackStyles';
import { useNavigate } from 'react-router-dom';
import { ReactComponent as Arrow } from '../../assets/icons/arrow.svg';

type ButtonBackProps = {
	route: string;
};

const ButtonBack: React.FC<ButtonBackProps> = ({ route }) => {
	const navigate = useNavigate();
	return (
		<Button onClick={() => navigate(route)}>
			<Arrow />
		</Button>
	);
};

export default ButtonBack;
