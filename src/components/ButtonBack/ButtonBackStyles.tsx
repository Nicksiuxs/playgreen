import styled from 'styled-components';

const Button = styled.button`
	font-size: 1.625rem;
	background: none;
	border: none;
	align-self: flex-start;

	& svg {
		fill: ${(props) => props.theme.iconColor};
	}
`;
export { Button };
