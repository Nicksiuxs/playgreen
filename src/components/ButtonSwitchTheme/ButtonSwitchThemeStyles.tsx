import styled from 'styled-components';

const Button = styled.button`
	background-color: ${(props) => props.theme.backgrounButtonSwitch};
	font-size: 1.625rem;
	border-radius: 1.125rem;
	padding: 0.9375rem 1.125rem 0.625rem 1.125rem;
	border: none;
	transition: all 0.5s ease;
`;

const ButtonAlternative = styled.button`
	background: rgba(34, 34, 67, 0.2);
	font-size: 1.625rem;
	border-radius: 1.125rem;
	padding: 0.9375rem 1.125rem 0.625rem 1.125rem;
	border: none;
`;

export { ButtonAlternative };
export default Button;
