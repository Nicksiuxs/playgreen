import React from 'react';
import { Div, ButtonCancel,ButtonLike } from './ButtonsActionsStyles';

import { ReactComponent as X } from '../../assets/icons/x.svg';
import { ReactComponent as Hearth } from '../../assets/icons/heart.svg';

type ButtonActionsProps = {
	handleLike : ()=>void
	handleDislike: ()=>void
}

const ButtonsActions:React.FC<ButtonActionsProps> = ({handleLike,handleDislike}) => {
	return (
		<Div>
			<ButtonCancel onClick={handleDislike}>
				<X />
			</ButtonCancel>
			<ButtonLike onClick={handleLike}>
				<Hearth />
			</ButtonLike>
		</Div>
	);
};

export default ButtonsActions;
