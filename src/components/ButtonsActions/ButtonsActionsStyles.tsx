import styled from 'styled-components';

const Div = styled.div`
	position: absolute;
	display: flex;
	bottom: 6.375rem;
	width: 100%;
	justify-content: center;
	align-items: center;
	padding-bottom: 2.9375rem;
	gap: 1.375rem;
`;

const ButtonCancel = styled.button`
	background-color: blue;
	border: none;
	border-radius: 100%;
	padding: 12px;
	display: flex;
	align-items: center;
	justify-content: center;
	box-shadow: 0px 10px 25px rgba(0, 0, 0, 0.08);
	background-color: ${(props) => props.theme.backgroundButtonDislike};
	& svg {
		height: 20px;
		width: 20px;

		& path{
			fill: ${(props) => props.theme.iconButtonDislike};;
		}
	}
`;

const ButtonLike = styled.button`
	display: flex;
	align-items: center;
	justify-content: center;
	background: linear-gradient(125.02deg, #236BFE -17.11%, #063BA8 98.58%);
	border: none;
	padding: 24px;
	border-radius: 100%;

	& svg{
		width: 36px;
		height: 35px;
	}
`;

export { Div, ButtonCancel,ButtonLike };
