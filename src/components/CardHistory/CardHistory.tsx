import React from 'react';
import { Card } from './CardHistoryStyles';
import { ReactComponent as X } from '../../assets/icons/x.svg';
import { ReactComponent as Hearth } from '../../assets/icons/heart.svg';

type CardHistoryProps = {
	title: string;
	image: string;
	like: boolean;
};

const defaultImg =
	'https://elcomercio.pe/resizer/VtSCWu5reTi7vSdepNUonGVBCtA=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/TWI2EWKUC5FH3MIFADCXX2RGBE.png';
const CardHistory: React.FC<CardHistoryProps> = ({ title, image = defaultImg, like }) => {
	return (
		<Card>
			<figure>
				<img src={image} alt={title} />
				<h2>{title}</h2>
			</figure>
			<div className="icon">
				{like ? (
					<div className="hearth">
						<Hearth />
					</div>
				) : (
					<X />
				)}
			</div>
		</Card>
	);
};

export default CardHistory;
