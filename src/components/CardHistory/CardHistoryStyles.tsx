import styled from 'styled-components';

const Card = styled.div`
	background-color: ${(props) => props.theme.backgroundCard};
	max-height: 4.8125rem;
	height: 4.8125rem;
	display: flex;
	align-items: center;
	width: 100%;
	border-radius: 0.75rem;
	justify-content: space-between;

	& figure {
		height: inherit;
		width: 100%;
		position: relative;

		& img {
			width: 100%;
			height: 100%;
			object-fit: cover;
			border-radius: 0.75rem;
		}

		& h2 {
			position: absolute;
			top: 0;
			height: 77px;
			z-index: 4;
			padding: 1.5rem 0 0 0.9375rem;
			width: 100%;
			border-radius: 0.75rem;
			font-family: 'DM Sans';
			font-style: normal;
			font-weight: 700;
			font-size: 1.5rem;
			color: #fefefe;
			background: rgba(0, 0, 0, 0.51);
		}
	}

	& .icon {
		display: flex;
		justify-content: center;
		align-items: center;
		max-width: 4.25rem;
		width: 4.25rem;
		& .hearth {
			svg {
				& path {
					fill: ${(props) => props.theme.iconHistoryLike};
				}
			}
		}
	}
`;

export { Card };
