import React from 'react';

import ButtonSwitchTheme from '../ButtonSwitchTheme/ButtonSwitchTheme';

import HeaderTools from './HeaderStyles';

const Header: React.FC = () => {
	return (
		<HeaderTools>
			<ButtonSwitchTheme />
		</HeaderTools>
	);
};

export default Header;
