import styled from 'styled-components';

const HeaderTools = styled.header`
	background-color: none;
	padding: 1.375rem 1.4375rem 0 1.3125rem;
	position: absolute;
	top: 0;
	z-index: 5;
	display: flex;
	justify-content: space-between;
	align-items: center;
	width: 100%;
`;

export default HeaderTools;
