import React from 'react';
import Div from './InputWithLabelStyles';

type props = {
	id: string;
	text: string;
	type: string;
	handleOnChange: (_e: React.FormEvent<HTMLInputElement>) => void;
	complete?: string;
};

const InputWithLabel: React.FC<props> = ({ id, text, type, handleOnChange, complete }) => {
	return (
		<Div>
			<label htmlFor={id}>{text}</label>
			<input type={type} id={id} name={id} onChange={handleOnChange} autoComplete={complete ? complete : 'off'} />
		</Div>
	);
};

export default InputWithLabel;
