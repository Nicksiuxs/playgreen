import React from 'react';
import CardHistory from '../CardHistory/CardHistory';
import { List } from './ListCardStyles';

type ListCardProps = {
	sports: any;
};

const ListCard: React.FC<ListCardProps> = ({ sports }) => {
	return (
		<List>
			{sports.map((result: any) => {
				return (
					<li key={result.sportId}>
						<CardHistory title={result.sportName} image={result.url} like={result.like} />
					</li>
				);
			})}
		</List>
	);
};

export default ListCard;
