import styled from 'styled-components';

const List = styled.ul`
	width: 100%;
	max-height: 60vh;
	overflow: hidden;
	overflow-y: scroll;

	& li {
		list-style: none;
		margin-bottom: 0.875rem;

		&:last-child {
			margin-bottom: 0;
		}
	}
`;

export { List };
