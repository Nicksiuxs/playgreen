import React from 'react';
import { Sport } from '../../types/sports';
import SportCard from '../SportCard/SportCard';

type ListSportsProps = {
	sports: Sport[];
};

const ListSports: React.FC<ListSportsProps> = ({ sports }) => {
	return (
		<>
			{sports.map((sport) => {
				return <SportCard key={sport.id} {...sport} />;
			})}
		</>
	);
};

export default ListSports;
