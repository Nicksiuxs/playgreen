import React from 'react';

import { ReactComponent as Menu } from '../../assets/icons/menu.svg';
import { ReactComponent as Watch } from '../../assets/icons/watch.svg';
import { ReactComponent as Notes } from '../../assets/icons/logout.svg';

import NavButton from '../NavButton/NavButton';

import Navbar from './NavBarStyles';

const NavBar = () => {
	return (
		<Navbar>
			<div>
				<NavButton route="/" icon={<Menu />} />
				<NavButton route="/history" icon={<Watch />} />
				<NavButton route="/settings" icon={<Notes />} />
			</div>
		</Navbar>
	);
};

export default NavBar;
