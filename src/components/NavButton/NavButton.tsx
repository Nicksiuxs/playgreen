import React from 'react';
import { useNavigate, useLocation } from 'react-router';

import ButtonNav from './NavButtonStyles';

type props = {
	icon: JSX.Element;
	route: string;
};

const NavButton: React.FC<props> = ({ icon, route }) => {
	const navigate = useNavigate();
	const location = useLocation();
	return (
		<ButtonNav isActive={location.pathname === route ? true : false} onClick={() => navigate(route)}>
			{icon}
		</ButtonNav>
	);
};

export default NavButton;
