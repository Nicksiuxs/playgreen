import styled from 'styled-components';

type props = {
	isActive?: boolean;
};

const ButtonNav = styled.button<props>`
	background: ${(props) => (props.isActive ? props.theme.backgroundNavButton : 'none')};
	font-size: 1.625rem;
	border-radius: 1.125rem;
	padding: 0.9375rem 1.125rem 0.625rem 1.125rem;
	border: none;

	& svg {
		& path {
			fill: ${(props) => (props.isActive ? props.theme.iconActive : props.theme.iconColor)};
		}
	}
`;

export default ButtonNav;
