import React from 'react';
import { Navigate } from 'react-router-dom';
import { useAppSelector } from '../../hooks/reduxHooks';

type PrivateRoutesProps = {
	children: JSX.Element;
};

const PrivateRoutes: React.FC<PrivateRoutesProps> = ({ children }: PrivateRoutesProps) => {
	const user = useAppSelector((state) => state.auth);
	return user.email ? children : <Navigate to="login" />;
};

export default PrivateRoutes;
