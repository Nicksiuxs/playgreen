import React from 'react';
import { Card } from './SportCardStyles';
import { Sport } from '../../types/sports';

const SportCard: React.FC<Sport> = (sport: Sport) => {
	return (
		<Card>
			<img src={sport.url} alt={sport.name} loading="lazy" />
			<h2>{sport.name}</h2>
		</Card>
	);
};

export default SportCard;
