import styled from 'styled-components';

const Card = styled.div`
	position: absolute;
	width: 100%;
	height: 35.25rem;
	border-radius: 2rem;

	& img {
		width: 100%;
		height: 100%;
		object-fit: cover;
		border-radius: 2rem;
	}

	& h2 {
		position: absolute;
		bottom: 0;
		width: 100%;
		padding: 26px 21px;
		font-family: 'DM Sans';
		font-style: normal;
		font-weight: 700;
		font-size: 2.125rem;
		background: linear-gradient(360deg, #000000 0%, #000000 58.85%, rgba(0, 0, 0, 0) 100%);
		color: #fefefe;
		border-radius: 0 0 2rem 2rem;
	}
`;

export { Card };
