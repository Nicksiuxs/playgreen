import React, { useContext, useState } from 'react';
import { ThemeProvider as ThemeProviderStyled } from 'styled-components';
import { Theme } from '../types/app';
import { createContext } from 'react';

const LightTheme: Theme = {
	backgoundTextContainer: '#F3F3F3',
	backgrounButtonSwitch: '#FFFFFF',
	backgroundInput: '#FFFFFF',
	backgroundNavButton: '#F5F5F5',
	backgroundCard: '#FFFFFF',
	fontColorLabel: '#3C3C3C',
	fontColorText: '#232232',
	fontColorTitle: '#161617',
	iconActive: '#1A5BE1',
	iconColor: '#EDEDED',
	iconButtonDislike:'#D36060',
	backgroundButtonDislike:'#FFFFFF',
	iconHistoryLike: '#2067F8',
	navbarBackground: '#FFFFFF',
	pageBackground: '#F3F3F3',
};

const DarkTheme: Theme = {
	backgoundTextContainer: '#2C2B3E',
	backgrounButtonSwitch: '#222243',
	backgroundInput: '##2F2F43',
	backgroundNavButton: '#1F1F31',
	backgroundCard: '#212135',
	fontColorLabel: '#FEFEFE',
	fontColorText: '#FEFEFE',
	fontColorTitle: '#FEFEFE',
	iconActive: '#FFFFFF',
	iconColor: '#777777',
	iconButtonDislike:'#FFFFFF',
	backgroundButtonDislike:'#222243',
	iconHistoryLike: '#FFFFFF',
	navbarBackground: '#2C2B3E',
	pageBackground: '#181828',
};

//TODO fix type here
const themes: any = {
	light: LightTheme,
	dark: DarkTheme,
};

// TODO fix any
const ThemeContext: any = createContext(LightTheme);

const useTheme: any = () => {
	return useContext(ThemeContext);
};

type ThemeProps = {
	children: React.ReactNode;
};

const ThemeProvider: React.FC<ThemeProps> = ({ children }: ThemeProps) => {
	const [theme, setTheme] = useState('light');
	return (
		<ThemeContext.Provider value={{ theme, setTheme, themes }}>
			<ThemeProviderStyled theme={themes[theme]}>{children}</ThemeProviderStyled>
		</ThemeContext.Provider>
	);
};

export { useTheme };
export default ThemeProvider;
