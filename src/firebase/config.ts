// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
	apiKey: 'AIzaSyD3vOpplXg2Efk61YUc0f30Tsi0_AJFn8M',
	authDomain: 'playgreen-52b20.firebaseapp.com',
	projectId: 'playgreen-52b20',
	storageBucket: 'playgreen-52b20.appspot.com',
	messagingSenderId: '74021019843',
	appId: '1:74021019843:web:f269af21163e14667f0276',
};

const app = initializeApp(firebaseConfig);
const auth = getAuth();
const db = getFirestore();

export { app, auth, db };
