import { SportReponse, Sport } from '../types/sports';

const filterSports = (sports: SportReponse[]) => {
	return sports
		.map((sport: SportReponse) => {
			if (sport.relationships.images && sport.relationships.images.data[0]) {
				return {
					id: sport.id,
					name: sport.attributes.name,
					url: sport.relationships.images.data[0].url,
				};
			}
		})
		.filter((sport: Sport | undefined) => sport !== undefined);
};

const getRandomNumber = (min: number, max: number): number => {
	return Math.floor(Math.random() * (max - min + 1) + min);
};

export { filterSports, getRandomNumber };
