import { useState } from 'react';
import { db } from '../firebase/config';
import { query, collection, where, getDocs, addDoc } from 'firebase/firestore';

const useCollection = (table: string) => {
	const collectionRef = collection(db, table);

	const [results, setResults] = useState<any>([]);
	const [error, setError] = useState<any>(null);
	const [isPending, setIsPending] = useState(false);

	const getAllLikes = async (condition: any[]) => {
		setResults([]);
		let resDoc = null;
		let q = null;

		q = query(collectionRef, where(condition[0], condition[1], condition[2]));
		resDoc = await getDocs(q);

		resDoc.forEach((doc) => {
			setResults((list: any) => [...list, { ...doc.data() }]);
		});
	};

	const add = async (doc: any) => {
		setError(null);
		setIsPending(true);
		try {
			const resDoc = await addDoc(collectionRef, doc);
			console.log('document ID: ' + resDoc.id);
			setIsPending(false);
			return resDoc;
		} catch (err: any) {
			console.log(err.message);
			setError('could not send the message');
			setIsPending(false);
			return null;
		}
	};

	return { error, isPending, results, add, getAllLikes };
};

export default useCollection;
