import React, { useState } from 'react';
import { UserCredentials } from '../types/user';

const useForm = (initialState: UserCredentials) => {
	const [formState, setFormState] = useState(initialState);

	const onInputChange = (event: React.FormEvent<HTMLInputElement>): void => {
		const { name, value } = event.currentTarget;
		setFormState({
			...formState,
			[name]: value,
		});
	};

	const onResetForm = (): void => {
		setFormState(initialState);
	};

	return {
		...formState,
		formState,
		onInputChange,
		onResetForm,
	};
};

export default useForm;
