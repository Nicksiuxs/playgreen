import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import ThemeProvider from './contexts/ThemeContext';
import { BrowserRouter } from 'react-router-dom';
import GlobalStyle from './GlobalStyles';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

root.render(
	<Provider store={store}>
		<BrowserRouter>
			<ThemeProvider>
				<GlobalStyle />
				<App />
			</ThemeProvider>
		</BrowserRouter>
	</Provider>,
);
