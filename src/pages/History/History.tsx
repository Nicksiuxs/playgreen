import React, { useEffect } from 'react';
import HistoryBackground from './HistoryStyles';
import useCollection from '../../hooks/useCollection';
import { useAppSelector } from '../../hooks/reduxHooks';
import ButtonBack from '../../components/ButtonBack/ButtonBack';
import ListCard from '../../components/ListCards/ListCard';

const History = () => {
	const user = useAppSelector((state) => state.auth);
	const { results, getAllLikes } = useCollection('likes');

	const getHistory = async () => {
		if (user.uid !== undefined) {
			await getAllLikes(['userId', '==', user.uid]);
		}
	};

	useEffect(() => {
		getHistory();
	}, []);

	return (
		<HistoryBackground>
			<ButtonBack route="/" />
			<h1>History</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<span>17 december</span>
			<ListCard sports={results} />
		</HistoryBackground>
	);
};

export default History;
