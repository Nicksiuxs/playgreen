import styled from 'styled-components';

const HistoryBackground = styled.div`
	background-color: ${(props) => props.theme.backgoundTextContainer};
	width: 100vw;
	height: 100vh;
	max-height: 100vh;
	display: flex;
	flex-direction: column;
	align-items: center;
	padding: 3.375rem 2rem 0 2rem;

	& h1 {
		width: 100%;
		font-family: 'DM Sans';
		font-style: normal;
		font-weight: 700;
		font-size: 2.625rem;
		color: ${(props) => props.theme.fontColorTitle};
		text-align: left;
		margin-top: 1.125rem;
		margin-bottom: 0.5rem;
	}

	& p {
		font-family: 'Epilogue';
		font-style: normal;
		font-weight: 400;
		font-size: 1.125rem;
		color: ${(props) => props.theme.fontColorText};
		margin-bottom: 0.5rem;
		width: 100%;
	}

	& span {
		font-family: 'Epilogue';
		font-style: normal;
		font-weight: 600;
		font-size: 0.875rem;
		text-align: left;
		width: 100%;
		color: ${(props) => props.theme.fontColorText};
		margin-bottom: 0.875rem;
	}
`;

export default HistoryBackground;
