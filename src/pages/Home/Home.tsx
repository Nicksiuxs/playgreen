import React, { useEffect } from 'react';
import Header from '../../components/Header/Header';
import HomeBackground from './HomeStyles';
import useCollection from '../../hooks/useCollection';
import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';
import { getSports } from '../../redux/thunks/sportsThunk';
import ButtonsActions from '../../components/ButtonsActions/ButtonsActions';
import SportCard from '../../components/SportCard/SportCard';
import { Sport } from '../../types/sports';
import { updateSport } from '../../redux/slices/sportsSlice';

const Home: React.FC = () => {
	const { add } = useCollection('likes');
	const user = useAppSelector((state) => state.auth);
	const { currentSport } = useAppSelector((state) => state.sports);
	const dispatch = useAppDispatch();

	const handleGetSports = () => {
		dispatch(getSports());
	};

	const handleLike = async (sport: Sport, like: boolean) => {
		if (user.uid !== undefined) {
			const newSport = {
				userId: user.uid,
				sportName: currentSport?.name,
				sportId: currentSport?.id,
				url: currentSport?.url,
			};
			await add({ like: like, ...newSport });
			await dispatch(updateSport());
		}
	};

	useEffect(() => {
		handleGetSports();
	}, []);

	return (
		<HomeBackground>
			<Header />
			{currentSport ? (
				<>
					<SportCard id={currentSport?.id} name={currentSport?.name} url={currentSport?.url} />
					<ButtonsActions
						handleDislike={() => handleLike(currentSport, false)}
						handleLike={() => handleLike(currentSport, true)}
					/>
				</>
			) : null}
		</HomeBackground>
	);
};

export default Home;
