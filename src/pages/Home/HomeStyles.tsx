import styled from 'styled-components';

const HomeBackground = styled.div`
	background-color: ${(props) => props.theme.pageBackground};
	height: 100vh;
	width: 100vw;
`;

export default HomeBackground;
