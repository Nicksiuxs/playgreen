import React, { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import InputWithLabel from '../../components/InputWithLabel/InputWithLabel';
import ButtonLink from '../../components/ButtonWithLink/ButtonWithLinkStyles';
import LoginBackground from './LoginStyles';

import useForm from '../../hooks/useForm';
import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';
import { loginAuth } from '../../redux/thunks/authThunk';

const Login: React.FC = () => {
	const { formState, onInputChange } = useForm({
		email: '',
		password: '',
	});

	const navigate = useNavigate();
	const dispatch = useAppDispatch();
	const user = useAppSelector((state) => state.auth);

	const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		dispatch(loginAuth(formState));
	};

	useEffect(() => {
		if (user.email) {
			navigate('/');
		}
	}, [user]);

	return (
		<LoginBackground>
			<h1>Welcome</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<form onSubmit={handleSubmit}>
				<InputWithLabel id="email" type="email" text="User" handleOnChange={onInputChange} />
				<InputWithLabel id="password" type="password" text="Password" handleOnChange={onInputChange} />
				<Link to="/reset-password">Forgot your password?</Link>
				<Link to="/signup">Create an account</Link>
				<ButtonLink>Login</ButtonLink>
			</form>
		</LoginBackground>
	);
};

export default Login;
