import React from 'react';

import InputWithLabel from '../../components/InputWithLabel/InputWithLabel';
import ButtonLink from '../../components/ButtonWithLink/ButtonWithLinkStyles';
import LoginBackground from '../Login/LoginStyles';

import useForm from '../../hooks/useForm';
import { useAppDispatch } from '../../hooks/reduxHooks';
import { resetPasswordAuth } from '../../redux/thunks/authThunk';
import ButtonBack from '../../components/ButtonBack/ButtonBack';

const ResetPassword: React.FC = () => {
	const { formState, onInputChange } = useForm({
		email: '',
	});

	const dispatch = useAppDispatch();

	const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		dispatch(resetPasswordAuth(formState));
	};

	return (
		<LoginBackground>
			<ButtonBack route="/login" />
			<h1>Reset Password</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<form onSubmit={handleSubmit}>
				<InputWithLabel id="email" type="email" text="email" handleOnChange={onInputChange} />
				<ButtonLink>Reset password</ButtonLink>
			</form>
		</LoginBackground>
	);
};

export default ResetPassword;
