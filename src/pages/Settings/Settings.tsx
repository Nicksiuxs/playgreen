import React from 'react';
import SettingsBackground from './SettingsStyles';
import ButtonBack from '../../components/ButtonBack/ButtonBack';
import ButtonLink from '../../components/ButtonWithLink/ButtonWithLinkStyles';
import { useAppDispatch } from '../../hooks/reduxHooks';
import { logout } from '../../redux/slices/authSlice';

const Settings = () => {
	const dispatch = useAppDispatch();

	const handleLogout = () => {
		console.log('cerrando sesión');
		dispatch(logout());
	};
	return (
		<SettingsBackground>
			<ButtonBack route="/" />
			<div>
				<h1>Logout</h1>
				<p>Click on the button to log out.</p>
				<ButtonLink onClick={handleLogout}>Logout</ButtonLink>
			</div>
		</SettingsBackground>
	);
};

export default Settings;
