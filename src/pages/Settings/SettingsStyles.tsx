import styled from 'styled-components';

const SettingsBackground = styled.div`
	background-color: ${(props) => props.theme.backgoundTextContainer};
	width: 100vw;
	height: 100vh;
	max-height: 100vh;
	display: flex;
	flex-direction: column;
	align-items: center;
	padding: 3.375rem 2rem 0 2rem;

	& div {
		height: 100%;
		width: 100%;
		& h1 {
			width: 100%;
			font-family: 'DM Sans';
			font-style: normal;
			font-weight: 700;
			font-size: 2.625rem;
			color: ${(props) => props.theme.fontColorTitle};
			text-align: left;
			margin-top: 1.125rem;
			margin-bottom: 1rem;
		}

		& p {
			font-family: 'Epilogue';
			font-style: normal;
			font-weight: 400;
			font-size: 1.125rem;
			color: ${(props) => props.theme.fontColorText};
			margin-bottom: 2rem;
			width: 100%;
		}

		& button {
			width: 100%;
		}
	}
`;

export default SettingsBackground;
