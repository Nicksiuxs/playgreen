import { createSlice } from '@reduxjs/toolkit';

interface UserState {
	email: string;
	uid?: string;
	showAlert?: boolean;
	messageAlert?: string;
}

const initialState: UserState = {
	email: '',
};

export const authSlice = createSlice({
	name: 'auth',
	initialState,
	reducers: {
		register: (state, action) => {
			state.email = action.payload.email;
		},
		login: (state, action) => {
			state.email = action.payload.email;
			state.uid = action.payload.uid;
		},
		resetPassword: (state) => {
			state.showAlert = true;
		},
		logout: (state) => {
			state.email = initialState.email;
			state.uid = '';
		},
	},
});

export const { register, login, resetPassword, logout } = authSlice.actions;
