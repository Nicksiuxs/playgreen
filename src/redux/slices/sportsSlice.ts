import { createSlice } from '@reduxjs/toolkit';
import { Sport } from '../../types/sports';
import { getRandomNumber } from '../../helpers/sports';

const initialState: { data: Sport[]; currentSport: Sport | null } = {
	data: [],
	currentSport: null,
};

export const sportsSlice = createSlice({
	name: 'sport',
	initialState,
	reducers: {
		setSports: (state, action) => {
			state.data = action.payload;
			state.currentSport = action.payload[getRandomNumber(0, action.payload.length - 1)];
		},
		updateSport: (state) => {
			state.currentSport = state.data[getRandomNumber(0, state.data.length - 1)];
		},
	},
});

export const { setSports, updateSport } = sportsSlice.actions;
