import { createUserWithEmailAndPassword, signInWithEmailAndPassword, sendPasswordResetEmail } from 'firebase/auth';
import { auth } from '../../firebase/config';
import { register, login, resetPassword } from '../slices/authSlice';
import { UserCredentials } from '../../types/user';

export const registerAuth = ({ email, password }: UserCredentials) => {
	//TODO change type here
	return async (dispatch: any) => {
		if (password !== undefined) {
			const response = await createUserWithEmailAndPassword(auth, email, password);

			if (response) {
				const user = response.user;
				console.log(user);
				dispatch(register({ email: user.email }));
			} else {
				//TODO Handle error
				console.log('Manejando un error');
			}
		}
	};
};

export const loginAuth = ({ email, password }: UserCredentials) => {
	return async (dispatch: any) => {
		if (password !== undefined) {
			const response = await signInWithEmailAndPassword(auth, email, password);

			if (response) {
				const user = response.user;
				console.log(user);
				dispatch(login({ email: user.email, uid: user.uid }));
			} else {
				console.log('error');
			}
		}
	};
};

export const resetPasswordAuth = ({ email }: UserCredentials) => {
	return async (dispatch: any) => {
		await sendPasswordResetEmail(auth, email);
		dispatch(resetPassword());
	};
};
