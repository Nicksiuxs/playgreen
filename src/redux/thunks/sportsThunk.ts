import axios from 'axios';
import { filterSports } from '../../helpers/sports';
import { setSports } from '../slices/sportsSlice';

export const getSports = () => {
	return async (dispatch: any) => {
		try {
			const response = await axios.get('https://sports.api.decathlon.com/sports');
			const sports = filterSports(response.data.data);
			dispatch(setSports(sports));
		} catch (error) {
			console.error(error);
		}
	};
};
