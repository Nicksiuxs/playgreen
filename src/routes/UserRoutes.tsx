import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Home from '../pages/Home/Home';
import Settings from '../pages/Settings/Settings';
import History from '../pages/History/History';
import NavBar from '../components/NavBar/NavBar';

const UserRoutes: React.FC = () => {
	return (
		<>
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="history" element={<History />} />
				<Route path="settings" element={<Settings />} />
			</Routes>
			<NavBar />
		</>
	);
};

export default UserRoutes;
