type Theme = {
	backgoundTextContainer: string;
	backgrounButtonSwitch: string;
	backgroundInput: string;
	backgroundNavButton: string;
	backgroundCard: string;
	fontColorLabel: string;
	fontColorText: string;
	fontColorTitle: string;
	iconActive: string;
	iconColor: string;
	iconHistoryLike: string;
	backgroundButtonDislike: string;
	iconButtonDislike:string;
	navbarBackground: string;
	pageBackground: string;
};

export type { Theme };
