type data = {
	url: string;
};

interface SportReponse {
	id: string;
	attributes: { name: string };
	relationships: { images?: { data: data[] } };
}

interface Sport {
	id: string;
	name: string;
	url?: string;
}

export type { SportReponse, Sport };
